package by.itstep.blogging.utils;

import java.sql.*;

public class ConnectionUtils {

    private static final String DB_URL = "jdbc:mysql://localhost:3306/blog";
    private static final String DB_USERNAME = "root";
    private static final String DB_PASSWORD = "root";

    public static Connection getConnection() throws SQLException {
        Connection con = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
        return con;
    }

    public static void close(Connection con, Statement st, ResultSet rs) {
        try {
            if(rs != null) rs.close();
            if(st != null) st.close();
            if(con != null) con.close();
        } catch (SQLException ex) {
            System.out.println("Can't close!");
        }
    }


}
