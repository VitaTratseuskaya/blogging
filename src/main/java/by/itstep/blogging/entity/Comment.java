package by.itstep.blogging.entity;

import java.sql.Date;
import java.util.Objects;

public class Comment {

    private int commentId;
    private int postId;
    private String message;
    private String authorName;
    private int rating;
    private Date createdAt;

    public Comment() {
    }

    public Comment(int commentId, int postId, String message, String authorName, int rating, Date createdAt) {
        this.commentId = commentId;
        this.postId = postId;
        this.message = message;
        this.authorName = authorName;
        this.rating = rating;
        this.createdAt = createdAt;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment = (Comment) o;
        return commentId == comment.commentId && postId == comment.postId && rating == comment.rating && Objects.equals(message, comment.message) && Objects.equals(authorName, comment.authorName) && Objects.equals(createdAt, comment.createdAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(commentId, postId, message, authorName, rating, createdAt);
    }

    @Override
    public String toString() {
        return "Comment{" +
                "commentId=" + commentId +
                ", postId=" + postId +
                ", message='" + message + '\'' +
                ", authorName='" + authorName + '\'' +
                ", rating=" + rating +
                ", createdAt=" + createdAt +
                '}';
    }
}
