package by.itstep.blogging.repository;

import by.itstep.blogging.entity.Comment;
import by.itstep.blogging.utils.ConnectionUtils;


import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CommentRepository {

    private Connection con = null;
    private PreparedStatement st = null;
    private ResultSet rs = null;

    private static final String DB_INSERT = "INSERT INTO comment (post_id, message, author_name, rating, created_at) VALUES ( ?, ?, ?, ?, ?);";
    private static final String FIND_BY_ID = "SELECT * FROM comment WHERE comment_id = ?;";
    private static final String FIND_ALL = "SELECT * FROM comment;";
    private static final String FIND_ALL_BY_POST_ID = "SELECT * FROM comment WHERE post_id = ?;";
    private static final String UPDATE = "UPDATE comment SET post_id = ?, message = ?, author_name = ?, rating = ?, created_at = ? WHERE comment_id = ?;";
    private static final String DELETE = "DELETE FROM comment WHERE comment_id = ?;";

    public void create(Comment comment) {
        try {
            con = ConnectionUtils.getConnection();
            st = con.prepareStatement(DB_INSERT);
            st.setInt(1, comment.getPostId());
            st.setString(2, comment.getMessage());
            st.setString(3, comment.getAuthorName());
            st.setInt(4, comment.getRating());
            st.setDate(5, comment.getCreatedAt());

            int addComment = st.executeUpdate();
            System.out.println("Add comment -> " + addComment);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("Error on create comment!");
        }finally {
            ConnectionUtils.close(con, st, rs);
        }
    }

    public Comment findById(int id) {
        try {
            con = ConnectionUtils.getConnection();
            st = con.prepareStatement(FIND_BY_ID);
            st.setInt(1, id);
            rs = st.executeQuery();

            Comment foundComment = extract(rs);
            System.out.println("Found comment -> " + foundComment);

            return foundComment;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("Error find by ID!");
        } finally {
            ConnectionUtils.close(con, st, rs);
        }
    }

    public List<Comment> findAll() {
        try {
         con = ConnectionUtils.getConnection();
         st = con.prepareStatement(FIND_ALL);
         rs = st.executeQuery();

         List<Comment> foundComments = extractAll(rs);
         System.out.println(foundComments.size() + " comment were found!");
         return foundComments;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("Error find All");
        } finally {
            ConnectionUtils.close(con, st, rs);
        }
    }

    public List<Comment> findAllByPostId(int id){
        try {
            con = ConnectionUtils.getConnection();
            st = con.prepareStatement(FIND_ALL_BY_POST_ID);
            st.setInt(1, id);
            rs = st.executeQuery();

            List<Comment> foundCommentAllByPostId = extractAll(rs);
            System.out.println(foundCommentAllByPostId.size() +
                    " comment were found by postId " + id);
            return foundCommentAllByPostId;
        }catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("Error find all by post!");
        }finally {
            ConnectionUtils.close(con, st, rs);
        }
    }

    public void upDate(Comment comment){
        try{
            con = ConnectionUtils.getConnection();
            st = con.prepareStatement(UPDATE);
            st.setInt(1, comment.getPostId());
            st.setString(2, comment.getMessage());
            st.setString(3, comment.getAuthorName());
            st.setInt(4, comment.getRating());
            st.setDate(5, comment.getCreatedAt());

            int upDate = st.executeUpdate();
            System.out.println(upDate + " comment update");
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("Error update comment!");
        } finally {
            ConnectionUtils.close(con, st, rs);
        }
    }

    public void deleteById(int id) {
        try{
            con = ConnectionUtils.getConnection();
            st = con.prepareStatement(DELETE);
            st.setInt(1, id);

            int deleteComment = st.executeUpdate();
            System.out.println(deleteComment + " comment was deleted!");
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("Error delete comment!", ex);
        } finally {
            ConnectionUtils.close(con, st, rs);
        }
    }

    private Comment extract(ResultSet rs) {
        try {
            if (rs.next()) {
                int commentId = rs.getInt("comment_id");
                int postId = rs.getInt("post_id");
                String message = rs.getString("message");
                String authorName = rs.getString("author_name");
                int rating = rs.getInt("rating");
                Date createdAt = rs.getDate("created_at");

                Comment foundComment = new Comment(commentId, postId, message, authorName, rating, createdAt);
                System.out.println("Found comment -> " + foundComment);

                return foundComment;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("Error extract!");
        } finally {
            ConnectionUtils.close(con, st, rs);
        }
        return null;
    }

    private List<Comment> extractAll(ResultSet rs) {
        try{
            List<Comment> foundComments = new ArrayList<>();
            while(rs.next()){
                int commentId = rs.getInt("comment_id");
                int postId = rs.getInt("post_id");
                String message = rs.getString("message");
                String authorName = rs.getString("author_name");
                int rating = rs.getInt("rating");
                Date createdAt = rs.getDate("created_at");

                Comment foundComment = new Comment(commentId, postId, message, authorName, rating, createdAt);
                foundComments.add(foundComment);
            }
            return foundComments;
        }catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("Error extractAll", ex);
        }finally {
            ConnectionUtils.close(con, st, rs);
        }
    }


}
