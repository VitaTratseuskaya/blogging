package by.itstep.blogging.repository;


import by.itstep.blogging.entity.Post;
import by.itstep.blogging.utils.ConnectionUtils;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class PostRepository {

    private Connection con = null;
    private PreparedStatement st = null;
    private ResultSet rs = null;

    private static final String DB_INSERT = "INSERT INTO post (title, description, rating, author_name) VALUES (?, ?, ?, ?);";
    private static final String FIND_BY_ID = "SELECT * FROM post WHERE post_id = ?;";
    private static final String FIND_ALL = "SELECT * FROM post;";
    private static final String UPDATE = "UPDATE post SET title = ?, description = ?, rating = ?, author_name = ? WHERE post_id = ?;";
    private static final String DELETE = "DELETE FROM post WHERE post_id = ?;";
    private static final String ADD_RATING = "UPDATE post SET rating = rating + 1 WHERE post_id = ?;";
    private static final String MINUS_RATING = "UPDATE post SET rating = rating - 1 WHERE post_id = ?;";
    private static final String SEARCH_BY_TITLE = "SELECT * FROM post WHERE title LIKE = ?;";

    public void create(Post post) {
        try {
            con = ConnectionUtils.getConnection();
            st = con.prepareStatement(DB_INSERT);

            st.setString(1, post.getTitle());
            st.setString(2, post.getDescription());
            st.setInt(3, post.getRating());
            st.setString(4, post.getAuthorName());

            int savePost = st.executeUpdate();
            System.out.println("Add post -> " + savePost);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("Error in creating a post!");
        } finally {
            ConnectionUtils.close(con, st, rs);
        }
    }

    public Post findById(int id) {
        try {
            con = ConnectionUtils.getConnection();
            st = con.prepareStatement(FIND_BY_ID);
            st.setInt(1, id);
            rs = st.executeQuery();

            Post foundPost = extract(rs);
            System.out.println("Found post -> " + foundPost);
            return foundPost;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("Error find by ID!");
        } finally {
            ConnectionUtils.close(con, st, rs);
        }
    }

    public List<Post> findAll() {
        try {
            con = ConnectionUtils.getConnection();
            st = con.prepareStatement(FIND_ALL);
            rs = st.executeQuery();

            List<Post> foundPost = extractAll(rs);
            System.out.println(foundPost.size() + " posts were found");
            return foundPost;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("Error find all!");
        } finally {
            ConnectionUtils.close(con, st, rs);
        }
    }

    public void upDate(Post post) {
        try {
            con = ConnectionUtils.getConnection();
            st = con.prepareStatement(UPDATE);
            st.setString(1, post.getTitle());
            st.setString(2, post.getDescription());
            st.setInt(3, post.getRating());
            st.setString(4, post.getAuthorName());

            int upDate = st.executeUpdate();
            System.out.println(upDate + " update post");
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("Error update post!");
        } finally {
            ConnectionUtils.close(con, st, rs);
        }
    }

    public void deleteById(int id) {
        try {
            con = ConnectionUtils.getConnection();
            st = con.prepareStatement(DELETE);
            st.setInt(1, id);

            int deletePost = st.executeUpdate();
            System.out.println(deletePost + " post was deleted!");
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("Error delete post!", ex);
        } finally {
            ConnectionUtils.close(con, st, rs);
        }
    }

    private Post extract(ResultSet rs) {
        try {
            if (rs.next()) {
                int postId = rs.getInt("post_id");
                String title = rs.getString("title");
                String description = rs.getString("description");
                int rating = rs.getInt("rating");
                String authorName = rs.getString("author_name");

                Post foundPost = new Post(postId, title, description, rating, authorName);
                System.out.println("Found post -> " + foundPost);

                return foundPost;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("Error extract");
        } finally {
            ConnectionUtils.close(con, st, rs);
        }
        return null;
    }

    private List<Post> extractAll(ResultSet rs) {
        try {
            List<Post> foundPosts = new ArrayList<>();
            while (rs.next()) {
                int postId = rs.getInt("post_id");
                String title = rs.getString("title");
                String description = rs.getString("description");
                int rating = rs.getInt("rating");
                String authorName = rs.getString("author_name");

                Post foundPost = new Post(postId, title, description, rating, authorName);
                foundPosts.add(foundPost);
            }
            return foundPosts;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("Error extractAll!");
        } finally {
            ConnectionUtils.close(con, st, rs);
        }
    }

    public void addRating(Post post) {
        try {
            con = ConnectionUtils.getConnection();
            st = con.prepareStatement(ADD_RATING);
            st.setInt(1, post.getPostId());

            int postNewRating = st.executeUpdate();
            System.out.println("New rating: " + postNewRating);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("Error add rating!");
        } finally {
            ConnectionUtils.close(con, st, rs);
        }
    }

    public void minusRating(Post post) {
        try {
            con = ConnectionUtils.getConnection();
            st = con.prepareStatement(MINUS_RATING);
            st.setInt(1, post.getPostId());

            int minusRating = st.executeUpdate();
            System.out.println("New rating: " + minusRating);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("Error minus rating!");
        } finally {
            ConnectionUtils.close(con, st, rs);
        }
    }

    public List<Post> searchByTitle(String title) {
        System.out.println("hello ");
        try {
            con = ConnectionUtils.getConnection();
            st = con.prepareStatement(SEARCH_BY_TITLE);
            st.setString(1, title);
            rs = st.executeQuery();

            List<Post> searchPost = extractAll(rs);
            System.out.println("Search post -> " + searchPost);
            return searchPost;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("Error search!");
        }finally {
            ConnectionUtils.close(con, st, rs);
        }
    }



}
