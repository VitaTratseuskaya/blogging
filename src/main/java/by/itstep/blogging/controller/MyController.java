package by.itstep.blogging.controller;

import by.itstep.blogging.entity.Comment;
import by.itstep.blogging.entity.Post;
import by.itstep.blogging.repository.CommentRepository;
import by.itstep.blogging.repository.PostRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.List;

@Controller
public class MyController {

    private PostRepository postRepository = new PostRepository();
    private CommentRepository commentRepository = new CommentRepository();

    @GetMapping("posts")
    public String getAllPosts(Model model, Post post) {
        try {
            List<Post> foundPosts = postRepository.findAll();
            model.addAttribute("foundPosts", foundPosts);
            System.out.println("Found posts " + foundPosts);

            foundPosts.sort(Post::compareTo);

            return "posts";
        } catch (Exception ex) {
            ex.printStackTrace();
            return "error";
        }
    }

    @GetMapping("/posts/{id}")
    public String getPost(Model model, @PathVariable int id) {
        Post foundPost = postRepository.findById(id);
        model.addAttribute("rating", foundPost.getRating());
        model.addAttribute("foundPostId", foundPost.getPostId());
        model.addAttribute("postToShow", foundPost);
        model.addAttribute("foundTitle", foundPost.getTitle());

        System.out.println("Rating: " + foundPost.getRating());

       /* List<Post> searchPost = postRepository.searchByTitle(foundPost.getTitle());
        model.addAttribute("searchPost", searchPost);
        System.out.println("searchPost" + searchPost);*/

        List<Comment> foundComments = commentRepository.findAllByPostId(id);
        model.addAttribute("foundComments", foundComments);
        System.out.println("Found comments " + foundComments);

        Comment commentCreate = new Comment();
        model.addAttribute("commentCreate", commentCreate);

        return "single-post";
    }

    @GetMapping("create-post")
    public String getCreatePost() {
        return "create-post";
    }

    @PostMapping("/create-post")
    public String addPost(Model model, Post post) {
        Post newPost = new Post();
        model.addAttribute("newPost", newPost);
        postRepository.create(post);
        return "redirect:/posts";
    }

    @PostMapping("/create-comment")
    public String createComment(Comment comment) {
        comment.setCreatedAt(new Date(System.currentTimeMillis()));

        int postId = comment.getPostId();

        System.out.println(comment);
        commentRepository.create(comment);
        return "redirect:/posts/" + postId;
    }

    @GetMapping("/posts/{id}/minus")
    public String minusRating(Model model, @PathVariable Integer id) {
        Post foundPost = postRepository.findById(id);
        postRepository.minusRating(foundPost);

        model.addAttribute("minusRating", foundPost.getRating());

        System.out.println("Rating = " + foundPost.getRating());
        return "redirect:/posts/" + id;
    }

    @GetMapping("/posts/{id}/add")
    public String addRating(Model model, @PathVariable Integer id) {
        Post foundPost = postRepository.findById(id);
        postRepository.addRating(foundPost);

        model.addAttribute("addRating", foundPost.getRating());

        System.out.println("Rating = " + foundPost.getRating());
        return "redirect:/posts/" + id;
    }

    @PostMapping("/posts/{id}/search")
    public String searchByTitle(Model model, @PathVariable Integer id){
        Post foundPost = postRepository.findById(id);
        List<Post> searchPost = postRepository.searchByTitle(foundPost.getTitle());
        model.addAttribute("searchPost", searchPost);
        System.out.println("searchPost" + searchPost);
        return "redirect:/posts/" + id;

    }
    }